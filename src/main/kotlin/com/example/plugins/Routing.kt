package com.example.plugins

import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import top.xana.acg.parser.BimiAcgParser

fun Application.configureRouting() {
    val bimi = BimiAcgParser()

    routing {
        get("/") {
            call.respondText("Hello World!")
        }

        get("/bimi") {
            call.respond(bimi.getHomeBody())
        }
    }
}
