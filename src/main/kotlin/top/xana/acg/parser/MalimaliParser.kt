package top.xana.acg.parser

import top.xana.acg.model.Anime
import top.xana.acg.model.Banner
import top.xana.acg.model.Episode

/**
 * @Author: root
 * @Date: 2022/2/15 15:13
 * @Description: email: cv4096@qq.com
 */

private const val BASE_URL = "https://www.malimali6.com"

class MalimaliParser : AnimeParser(), SearchInterface, SourceInterface {
    /*
     url.hashCode map to document
     */

    fun getBanner(): List<Banner> {
        val aList = get(BASE_URL).selectXpath("//ul[@class='carousel']/li/a")
        return aList.map {
            Banner(
                title = it.attr("title"),
                uri = BASE_URL + it.attr("href"),
                cover = "url\\((.*?)\\)".toRegex().find(it.html())?.groupValues?.get(1)
            )
        }
    }

    fun getHot(): List<Anime> {
        val aList = get(BASE_URL).selectXpath("//div[@class='slader-r']/ul/div/a")
        return aList.map {
            val mark = it.selectXpath("//div[2]/p")
            Anime(
                title = mark[0].text(),
                uri = BASE_URL + it.attr("href"),
                cover = it.selectXpath("//div[1]/img").attr("src"),
                status = mark[1].text(),
                views = mark[2].selectXpath("//span[1]").text(),
                date = mark[2].selectXpath("//span[2]").text(),
                extra = mark.text(),

            )
        }
    }

    fun getRecommend(): List<Anime> {
        val aList = get(BASE_URL).selectXpath("//div[@id='movie_content_div']/ul/li")
        return aList.map {
            Anime(
                title = it.selectXpath("//figcaption/b").text(),
                uri = BASE_URL + it.selectXpath("//a").attr("href"),
                cover = it.selectXpath("//a/div[1]/img").attr("src"),
                status = it.selectXpath("//figcaption/p").text(),
                date = it.selectXpath("//div[@class='play_hv']").text(),
                views = "999"
            )
        }
    }

    /**
     * 0 日榜
     * 1 周榜
     * 2 总榜
     */
    fun getRank(): List<List<Anime>> {
        val aList = get(BASE_URL).selectXpath("//div[@id='mytab2r']/ul")
        return aList.map {
            val lia = it.selectXpath("//li/a")
            lia.map {
                Anime(
                    title = it.attr("title"),
                    uri = BASE_URL + it.attr("href"),
                    cover = it.selectXpath("//img").attr("data-echo")
                )
            }
        }
    }

    fun getAnime(): List<List<Anime>> {
        val aList = get(BASE_URL).selectXpath("//div[@class='column-lft']/div[2]/ul[1]")
        return aList.map {
            val lia = it.selectXpath("//li")
            lia.map {
                Anime(
                    title = it.selectXpath("//figcaption/b").text(),
                    uri = BASE_URL + it.selectXpath("//a").attr("href"),
                    cover = it.selectXpath("//a/img").attr("data-echo"),
                    status = it.selectXpath("//figcaption/p[2]").text(),
                    date = it.selectXpath("//div[@class='play_hv']").text(),
                    views = "999"
                )
            }
        }
    }

    override fun search(kw: String, p: Int): List<Anime> {
        val uri = "$BASE_URL/vodsearch/$kw----------$p---.html"
        val alist = get(uri).selectXpath("//li[@class='search list']")
        return alist.map {
            Anime(
                title = it.selectXpath("//a").attr("title"),
                uri = BASE_URL + it.selectXpath("//a").attr("href"),
                cover = it.selectXpath("//a//img").attr("data-echo"),
                status = it.selectXpath("//span[@class='so-imgTag_rb']").text(),
                extra = listOf(
                    it.selectXpath("//div[@class='des hide']").text(),
                    it.selectXpath("//div[@class='tags']").text()
                ),
            )
        }
    }

    override fun detail(uri: String): Anime {
        val body = get(uri).selectXpath("//div[@class='drama-box']")[0]
        val div = body.selectXpath("//div[@id='thumb']/img")
        return Anime(
            title = div.attr("alt"),
            cover = div.attr("src"),
            extra = body.selectXpath("//div[@class='drama-data']//label").text(),
        )
    }

    override fun source(uri: String): List<List<Episode>> {
        val req = get(uri).selectXpath("//div[@class='column-lft'][1]/div")
        return req.map {
            it.selectXpath("//ul/li/a").map {
                Episode(it.text(), BASE_URL + it.attr("href"))
            }
        }
    }

    override fun favor(uri: String): List<Anime> {
        val req = get(uri).selectXpath("//div[@id='mytab8r'][1]/ul/li")

        return req.map {
            val sl = it.selectXpath("//a").attr("title").split(" ")
            Anime(
                title = sl[0],
                uri = BASE_URL + it.selectXpath("//a").attr("href"),
                cover = it.selectXpath("//a/img").attr("data-echo"),
                status = sl[1],
                extra = it.selectXpath("//p").text(),
            )
        }
    }

    override fun playUrl(uri: String): String {
        val hl = get(uri).html()
        val url = "\\{\"flag\".+?\"url\":\"(.+?)\"".toRegex().find(hl)!!.groupValues[1]
        return url.replace("\\", "")
    }


}