package top.xana.acg.parser

import top.xana.acg.model.Anime
import top.xana.acg.model.Episode

/**
 * @Author: root
 * @Date: 2022/2/15 22:31
 * @Description: email: cv4096@qq.com
 */
interface SearchInterface {
    fun search(kw: String, p: Int = 1): List<Anime>
}


interface SourceInterface {
    fun detail(uri: String): Anime

    fun source(uri: String): List<List<Episode>>

    fun favor(uri: String): List<Anime>

    fun playUrl(uri: String): String
}