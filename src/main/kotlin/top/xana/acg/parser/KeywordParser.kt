package top.xana.acg.parser

/**
 * @Author: root
 * @Date: 2022/2/16 21:48
 * @Description: email: cv4096@qq.com
 */


private val KW_URL = "http://www.qimiqimi.co/index.php/ajax/suggest?mid=1&wd=%s"

object KeywordParser : Parser() {

    fun kw(kw: String): List<String> {
        val uri = KW_URL.format(kw)
        val json = get(uri).toString()
        val iter = "\"name\":\"(.+?)\"".toRegex().findAll(json).iterator()

        val ls = ArrayList<String>()
        while (iter.hasNext())
            ls.add(iter.next().groupValues[1])

        return ls
    }
}
