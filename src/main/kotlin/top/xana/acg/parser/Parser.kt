package top.xana.acg.parser

import org.jsoup.nodes.Document
import top.xana.acg.request.IRequest
import top.xana.acg.request.JsoupRequest

/**
 * @Author: root
 * @Date: 2022/2/16 21:52
 * @Description: email: cv4096@qq.com
 */
open class Parser : IParser, IRequest {

    protected val map = HashMap<Int, Document>(5)

    private val request = JsoupRequest()

    fun test(uri: String): String {
        return get(uri).toString()
    }

    override fun get(uri: String): Document {
        if (map.containsKey(uri.hashCode()))
            return map[uri.hashCode()]!!

        val rs = request.get(uri)
        map[uri.hashCode()] = rs
        return rs
    }

    override fun post(uri: String, params: Map<String, String>?): Document {
        return request.post(uri, params)
    }
}