package top.xana.acg.model

/**
 * Created by xana on 2022/1/11
 * Describe: hello world
 */

class Bangumi(
    var id: String,
    var cover: String?,
    var title: String?,
    var uri: String?,
    var status: String?,
    val extra: String?
) {

    constructor() : this("", null, null, null, null, null)

    override fun toString(): String {
        return "Bangumi(id=$id, cover=$cover, title=$title, uri=$uri, status=$status)"
    }


    /**
     * cover : https://tva3.sinaimg.cn/large/006MDjU7gy1g1qrbxibjnj305002tweh.jpg
     * title : 异界少女召唤术 【无修正】
     * uri : https://www.bimiacg4.net/bangumi/bi/336/
     * status : 全12话
     */

}