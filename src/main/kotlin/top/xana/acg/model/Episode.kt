package top.xana.acg.model

data class Episode(
    val title: String,
    val desc: String = ""
)