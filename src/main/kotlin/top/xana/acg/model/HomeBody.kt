package top.xana.acg.model

import kotlinx.serialization.Serializable

/**
 * @Author: root
 * @Date: 2022/2/15 21:54
 * @Description: email: cv4096@qq.com
 */
@Serializable
data class HomeBody(
    val label: String,
    val items: List<Anime>
)
