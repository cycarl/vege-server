package top.xana.acg.model

import kotlinx.serialization.Serializable


/**
 * @Author: root
 * @Date: 2022/2/15 16:14
 * @Description: email: cv4096@qq.com
 */
@Serializable
data class Anime(
    val title: String? = null,
    val uri: String = "",
    val cover: String? = null,
    val status: String? = null,
    val views: String? = null,
    val date: String? = null,
    val extra: Any? = null,
) {
    companion object {
        const val SOURCE_BANNER = 0b00
        const val SOURCE_COLLECTION = 0b01
        const val SOURCE_HOT = 0b10
        const val SOURCE_TIMELINE = 0b11
        const val SOURCE_COMMON = 0b100
    }

}


