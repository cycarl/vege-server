package top.xana.acg.model

/**
 * @Author: root
 * @Date: 2022/2/15 15:40
 * @Description: email: cv4096@qq.com
 */
data class Banner(
    val title: String,
    val uri: String,
    val cover: String?
)
