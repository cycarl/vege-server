package top.xana.acg.request

import org.jsoup.Jsoup
import org.jsoup.nodes.Document


/**
 * @Author: root
 * @Date: 2022/2/15 13:10
 * @Description: email: cv4096@qq.com
 */
class JsoupRequest : IRequest {
    override fun get(uri: String): Document {
        return Jsoup.connect(uri)
            .ignoreContentType(true)
            .header(
                "user-agent",
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.72 Safari/537.36"
            )
            .header("accept", "application/json, text/javascript, */*; q=0.01")
            .followRedirects(false)
            .get()
    }

    override fun post(uri: String, params: Map<String, String>?): Document {
        return Jsoup.connect(uri).apply {
            params?.forEach {
                data(it.key, it.value)
            }
        }.post()
    }
}