package top.xana.acg.request

import org.jsoup.nodes.Document


/**
 * @Author: root
 * @Date: 2022/2/15 13:08
 * @Description: email: cv4096@qq.com
 */
interface IRequest {
    fun get(uri: String): Document
    fun post(uri: String, params: Map<String, String>? = null): Document
}